"use strict";
const path = require('path');
const fs = require("fs");
const puppeteer = require('puppeteer');

let pages = [];
function pushToPages(currentPath) {
    fs.readdirSync(currentPath).forEach(file => {
        const fullPath = path.join(currentPath, file);
        if (fs.statSync(fullPath).isDirectory()) return pushToPages(fullPath);
        else if (fullPath.endsWith(".html")) return pages.push(fullPath.replace(__dirname + "/.final/", ""));
    });
}; pushToPages(__dirname + "/.final");

console.log(pages);

const statify = async (path) => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('http://localhost:7777/' + path, { waitUntil: 'networkidle0' });
    const data = await page.evaluate(() => document.querySelector('*').outerHTML);
    fs.writeFileSync('.final/' + path, data);
    await browser.close();
}; for (let page in pages) { statify(pages[page]); }