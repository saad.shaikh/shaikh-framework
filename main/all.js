const forAnchors = () => {
    for (i = 0; i < document.getElementsByTagName('a').length; i++) {
        document.getElementById(document.getElementsByTagName('a')[i].id).addEventListener('click', (event) => {
            event.preventDefault();
            const a = new XMLHttpRequest();
            const b = event.target.pathname;
            a.open('GET', b, true);
            a.onload = () => {
                if (a.status == 200) {
                    document.body.innerHTML = new DOMParser().parseFromString(a.responseText, "text/html").body.innerHTML;
                    document.title = new DOMParser().parseFromString(a.responseText, "text/html").title;
                    window.history.pushState(null, null, b);
                    repeater();
                }
            }
            a.onerror = () => console.log('Error occured while loading ' + b + ".");
            a.send();
        });
        if ((document.getElementsByTagName('a')[i].href === window.location.href) && (typeof i !== undefined)) {
            document.getElementsByTagName('a')[i].style.pointerEvents = "none";
        };
    }
};
forAnchors();
/*
const crudGETText = (source, target) => {
    event.preventDefault()
    let xhr = new XMLHttpRequest();
    console.log(xhr);
    xhr.open('GET', source, true);
    xhr.onprogress = () => document.getElementById(target).innerHTML = "Loading..";
    xhr.onload = () => {
        if (xhr.status == 200) {
            document.getElementById(target).innerHTML = xhr.responseText;
        }
    }
    xhr.onerror = () => console.log('Error occured!');
    xhr.send();
}

const getObject = (source, target) => {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', source, true);
    xhr.onload = () => {
        if (xhr.status == 200) {
            let user = JSON.parse(xhr.responseText);
            document.getElementById(target).getElementsByClassName('user-name')[0].innerHTML = `${user.name}`;
            document.getElementById(target).getElementsByClassName('user-email')[0].innerHTML = `${user.email}`;
        }
    }
    xhr.send();
}
*/
const getJSON = (source, target, specs, searchBy) => {
    if (document.getElementById(target + '-0')) {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', source, true);
        xhr.onload = () => {
            if (xhr.status == 200) {
                let db = JSON.parse(xhr.responseText).filter(
                    (o) => o.login.toLowerCase().includes(document.getElementById(target + '-search').value.toLowerCase())
                );
                for (d = 0; d < db.length; d++) {
                    if (!document.getElementById(target + '-' + d)) {
                        copy = document.getElementById(target + "-0").cloneNode(true);
                        copy.id = target + "-" + d;
                        const elements = copy.getElementsByTagName("*");
                        for (let e = 0; e < elements.length; e++) {
                            if (elements[e].id !== "") {
                                elements[e].id = elements[e].id.replace("-0", "-" + d);
                            }
                        }
                        document.getElementById(target + "-" + (d - 1)).parentNode.insertBefore(copy, document.getElementById(target + "-" + (d - 1)).nextSibling)
                    };
                    let elem = document.getElementById(target + '-' + d).getElementsByTagName('*');
                    for (let e = 0; e < elem.length; e++) {
                        const fill = db[d][elem[e].id.replace(target + '-', '').replace('-' + d, '')];
                        if (elem[e].tagName !== 'IMG') { elem[e].innerHTML = fill; }
                        else if (elem[e].tagName = 'IMG') { elem[e].src = fill; }
                    };
                };
                if (db.length > 0) {
                    let extra = db.length;
                    while (document.getElementById(target + '-' + extra)) { document.getElementById(target + '-' + extra).remove(); extra++; };
                } else if (db.length == 0) {
                    let elem = document.getElementById(target + '-0').getElementsByTagName('*');
                    for (let e = 0; e < elem.length; e++) {
                        if (elem[e].tagName !== 'IMG') { elem[e].innerHTML = "No data"; }
                        else if (elem[e].tagName = 'IMG') { elem[e].src = ""; elem[e].alt = "No data"; }
                    };
                    let extra = 1;
                    while (document.getElementById(target + '-' + extra)) { document.getElementById(target + '-' + extra).remove(); extra++; };
                }
            }
        }
        xhr.send();
    }
};

const buttonExternal = () => {
    for (let i = 0; i < document.getElementsByClassName('external').length; i++) {
        document.getElementsByClassName('external')[i].addEventListener('click', (event) => {
            window.open(event.target.innerHTML, '_blank').focus();
        })
    }
};

/* expand pdf */
const togglePDF = () => {
    if (document.getElementById("expand-pdf")) {
        document.getElementById("expand-pdf").addEventListener("click", function () {
            var pdf = document.getElementById("pdf-lecture");
            if (pdf.width == "490px") {
                pdf.height = "1277px";
                pdf.width = "100%";
                document.getElementById("pdf-description-p").style.display = "none";
                document.getElementById("expand-pdf").innerHTML = "Collapse PDF";
            } else {
                pdf.height = "618px";
                pdf.width = "490px";
                document.getElementById("pdf-description-p").style.display = "block";
                document.getElementById("expand-pdf").innerHTML = "Expand PDF";
            }
            document.getElementById("pdf-description").classList.toggle('pdf-description-expanded');
        });
    }
}

const repeater = () => {
    getJSON('https://api.github.com/users', 'api', null, ['login']);
    forAnchors();
    buttonExternal();
    togglePDF();

};
repeater();
window.onchange = () => { repeater(); };