# Shaikh Framework

**Shaikh Framework** is a web development that is built for *fast build times*, *fast run times* and *easy deployment*.
This is achieved by using the Shel Script in place of JavaScript in a number of crucial areas to enhance performance.


## Shortcuts:

- To compile the project, run the command: `./compile`
- To run the project, run the command: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; `./serve`
- To deploy the project, run the command: &nbsp;&nbsp; `./deploy`
- In you hosting provider's control panel, set the publish directory to `.final`


You can support me on Patreon at: [https://www.patreon.com/shaikhframework](https://www.patreon.com/shaikhframework)